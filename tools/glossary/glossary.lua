local xettelkasten_dir = os.getenv("KASTEN")
local lfs = require("lfs")
local M = {}

M.preamble = os.getenv("PREAMBLE")

local function debugging(msg)
    texio.write_nl("DEBUG: " .. msg)
end

debugging("sourced lua")
debugging("using preamble: "..M.preamble)

local function no_references()
    debugging("no references")
    tex.print("\\vspace{3pt}", "\\textit{no references}")
end

M.refs = function(zettel)
    local file = io.open(xettelkasten_dir..zettel.."/references", "r")
    if file == nil or file:lines() == nil then
        no_references()
        return
    end

    local lines = file:lines()
    local found_refs = false
    for line in lines do
        debugging("here is a line")
        if line ~= "" then
            found_refs = true
            line = line:gsub("_", " ")
            tex.print("$\\rightarrow$ \\hyperlink{"..line.."}{\\textit{"..line.."}} \\newline")
        end
    end

    if not found_refs then
        no_references()
    end

    collectgarbage("collect")
end

local function sort_alphabetical(a, b)
    return a:lower() < b:lower()
end

-- this function sucks, it should not be recursive
local function insert(file)
    local path = file .. "/zettel.tex"
    debugging("adding "..path)
    tex.print("\\input{"..path.."}")
end

M.insert_zettel = function()
    debugging("insert_zettel was called")
    local files = {}
    local healthy = io.open("../../build_cache/healthy", "r")

    if healthy == nil then
        return
    end

    debugging("debugging not nil!!!")

    for zettel in healthy:lines() do
        table.insert(files, zettel)
    end

    table.sort(files, sort_alphabetical)

    for _,file in pairs(files) do
        insert(file)
    end
end

M.tags = function()
    debugging("Adding tags (not implemented)")
end

return M
