source ./config

echo "generating tikz"

zettel_list="$("$CORE" ls)"

rm -rf ./build/graph.tikz
echo "\graph[spring layout, node distance=3cm, components go down left aligned, grow=right, nodes={text height=.5em, text depth=.18em, draw, align=center}]{" > ./build/graph.tikz
for zettel in $zettel_list; do
    refs="$("$CORE" ref ls -z "$zettel")"
    zettel="${zettel//_/ }"
    zettel="${zettel//-/ }"
    zettel="${zettel//./ }"
    # echo "$zettel," >> ./build/graph.tikz
    for ref in $refs; do
        ref="${ref//_/ }"
        ref="${ref//-/ }"
        ref="${ref//./ }"
        echo "$zettel--$ref," >> ./build/graph.tikz
    done
done

echo ";};" >> ./build/graph.tikz
