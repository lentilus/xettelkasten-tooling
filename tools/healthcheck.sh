
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    echo script has to be sourced from generate assets
    exit
fi
zettel_list="$($CORE ls)"
dir="$(pwd)"

# rm -rf build_cache/*

compile() {
    echo "compiling zettel: $1.tex"
    latexmk -pdf -f norc -pdflatex="lualatex -interaction=batchmode" -outdir="$2" "$1.tex" &> /dev/null
    [ -f "$2/zettel.pdf" ] && return 0
    echo "no pdf produced for  $2/zettel.tex"
    return 1
}

healthy=()
unhealthy=()

for zettel in $zettel_list; do
    path="$($CORE path -z "$zettel")"
    cd "$path" || exit
    if compile "$path/zettel" "$dir/build_cache/$zettel"; then
        healthy+=("$zettel")
    else
        unhealthy+=("$zettel")
    fi
done

cd "$dir" || exit
rm -rf ./build_cache/healthy && touch ./build_cache/healthy
for zettel in "${healthy[@]}"; do
    "$CORE" path -z "$zettel" >> ./build_cache/healthy
done
